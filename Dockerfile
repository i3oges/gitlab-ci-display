FROM node:latest AS builder

ENV GITLAB_URL=gitlab-proxy-dqj3ck2jda-uc.a.run.app

WORKDIR /app

COPY package.json package-lock.json ./

RUN npm ci

COPY angular.json tsconfig*.json extra-webpack.config.js ./
COPY src src

RUN npm run build

# Stage 2
FROM nginx:latest

COPY --from=builder /app/dist/gitlab-ci-display /usr/share/nginx/html

COPY nginx.conf /etc/nginx/nginx.conf

EXPOSE 8080

CMD [ "nginx", "-g", "daemon off;" ]