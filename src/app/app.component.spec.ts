import { Location } from '@angular/common';
import { DebugElement, NgZone, Component } from '@angular/core';
import { ComponentFixture, fakeAsync, TestBed, tick, async } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { Router, Routes } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { GitlabService } from './gitlab/gitlab.service';
import { GitlabServiceMock } from './testing/gitlab.service.mock';
import { SharedModule } from './shared/shared.module';

@Component({ template: 'Log' })
class MockLogComponent {}

@Component({ template: 'Group' })
class MockGroupComponent {}

@Component({ template: `<router-outlet></router-outlet>` })
class MockAppComponent {}

describe('AppComponent', () => {
  let fixture: ComponentFixture<AppComponent>;
  let toolbar: DebugElement;
  let router: Router;
  let location: Location;
  let gitlabService: GitlabService;
  let zone: NgZone;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule.withRoutes([
          { path: '', component: MockAppComponent },
          { path: 'group/:groupId', component: MockGroupComponent },
          { path: 'group/:groupId/project/:projectId/job/:jobId', component: MockLogComponent },
        ]),
        SharedModule,
      ],
      providers: [{ provide: GitlabService, useClass: GitlabServiceMock }],
      declarations: [AppComponent, MockAppComponent, MockGroupComponent, MockLogComponent],
    }).compileComponents();
    router = TestBed.inject(Router);
    location = TestBed.inject(Location);
    gitlabService = TestBed.inject(GitlabService);
    zone = new NgZone({ enableLongStackTrace: false });
    fixture = TestBed.createComponent(AppComponent);

    toolbar = fixture.debugElement.query(By.css('mat-toolbar'));
    fixture.detectChanges();
  }));

  it('should create the app', () => {
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it('should render title', () => {
    expect(toolbar).toBeTruthy();
    expect(toolbar.nativeElement.textContent).toBe('Pipeline Viewer');
  });

  it('should navigate to group with an id', fakeAsync(() => {
    zone.run(() => router.navigate(['group', 1]));
    tick();
    expect(location.path()).toBe('/group/1');
    fixture.detectChanges();
    const elm = fixture.debugElement;
    expect(elm.query(By.css('.breadcrumb')).nativeElement.textContent.trim()).toBe('/ Foobar Group');
  }));

  it('should not be able to navigate with just group and project', fakeAsync(() => {
    zone
      .run(() => router.navigate(['group', 1, 'project', 3]))
      .catch(err => {
        expect(err).toBeTruthy();
        expect(location.path()).toBe('/');
      });
  }));

  it('should navigate to job with group, project and job id', fakeAsync(() => {
    const completeWithBreadcrumbs = 'Pipeline Viewer / Foobar Group / Diaspora Project Site / rubocop';
    zone.run(() => router.navigate(['group', 1, 'project', 3, 'job', 8]));
    tick();
    expect(location.path()).toBe('/group/1/project/3/job/8');
    fixture.detectChanges();

    const [
      {
        nativeElement: { textContent: groupBreadcrumb },
      },
      {
        nativeElement: { textContent: jobBreadcrumb },
      },
    ] = fixture.debugElement.queryAll(By.css('.breadcrumb'));

    // replace &nbsp with ' ' with .replace method
    const noBreadcrumbElm = fixture.debugElement.query(By.css('.breadcrumb-no-link')).nativeElement.textContent.replace(/\u00a0/g, ' ');
    const toolbarText = fixture.debugElement.query(By.css('mat-toolbar')).nativeElement.textContent.replace(/\u00a0/g, ' ');

    expect(groupBreadcrumb.replace(/\u00a0/g, ' ')).toBe(' / Foobar Group');
    expect(jobBreadcrumb.replace(/\u00a0/g, ' ')).toBe(' / rubocop');
    expect(noBreadcrumbElm).toBe(' / Diaspora Project Site');
    expect(toolbarText).toBe(completeWithBreadcrumbs);
  }));

  it('should have no breadcrumbs when no job or group is being visited', fakeAsync(() => {
    zone.run(() => router.navigate(['']));
    tick();
    expect(location.path()).toBe('/');

    expect(fixture.debugElement.query(By.css('mat-toolbar')).nativeElement.textContent).toBe('Pipeline Viewer');
  }));
});
