# GitlabCiDisplay

## A simple presentation layer for Gitlab CI/CD pipelines

Easily view your pipelines in a style similar to that other site that starts with a "C".

Uses an [api proxy](https://gitlab.com/i3oges/gitlab-api-proxy) so you don't have to show your private access tokens to the world

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Jest](https://jestjs.io) and [Chai](https://chaijs.com).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Cypress](http://cypress.io/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
