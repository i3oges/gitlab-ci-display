module.exports = {
  globals: {
    nprocess: {
      env: {
        GITLAB_URL: "gitlab.com",
      },
    },
    "ts-jest": {
      allowSyntheticDefaultImports: true,
    },
  },
  coveragePathIgnorePatterns: ["/node_modules/", "/src/app/.+.mock(s)?.ts"],
  testPathIgnorePatterns: ["/cypress/", ".+.spec.js"],
};
